package com.shlee.compressiontest;

import android.app.Application;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

/**
 * Created by leesangho on 16. 5. 23..
 */
public class MyApp extends Application {


    private static final String TAG = "ffmpeg";

    @Override
    public void onCreate() {
        super.onCreate();

        FFmpeg ffmpeg = FFmpeg.getInstance(this);
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {

                }

                @Override
                public void onFailure() {
                    Log.d(TAG, "ffmpeg load failed.");
                }

                @Override
                public void onSuccess() {
                    Log.d(TAG, "ffmpeg load success.");
                }

                @Override
                public void onFinish() {

                }
            });
        } catch (FFmpegNotSupportedException e) {
            Log.e(TAG, "ffmpeg load failed : device achitecthure is not supported.", e);
        }

    }
}
