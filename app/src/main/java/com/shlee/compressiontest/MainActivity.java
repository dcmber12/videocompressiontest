package com.shlee.compressiontest;

import android.media.MediaMetadataRetriever;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "compress_test";
    private File[] list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "onCreate");

        findViewById(R.id.hello).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            startDir(new File("/storage/emulated/0/Download/test"));

            }
        });

    }

    private void startDir(File dir) {

        if (dir == null)
            throw new NullPointerException();

        if (!dir.isDirectory()) {
            Log.e(TAG,"not a directory");
            return;
        }
        list = dir.listFiles();
        handleDirFile(0);
    }

    final int KILO_BYTES = 1024;
    private void handleDirFile(final int fileNumber) {

        if (fileNumber >= list.length) {
            Log.i(TAG, "end.");
            return;
        }

        File file = list[fileNumber];

        Log.d(TAG, file.getName());
        boolean isExeed = false;
        if (file.length() / KILO_BYTES < 200) {
            isExeed = false;
            Log.d(TAG, "file size (kB) : " + file.length() / KILO_BYTES);
        } else {
            isExeed = true;
            Log.i(TAG, "file size exceed(kB) : " + file.length() / KILO_BYTES);
        }

        if (!isExeed) {
            //초과하지 않음.
            handleDirFile(fileNumber + 1);
            return;
        }

        MediaMetadataRetriever ret = new MediaMetadataRetriever();
        ret.setDataSource(file.getAbsolutePath());
        String durStr = ret.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        int width = Integer.valueOf(ret.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
        int height = Integer.valueOf(ret.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
        if (durStr == null || durStr.isEmpty()) {
            Log.e(TAG, "cannot retrive duration!");
            ret.release();
            return;
        }
        ret.release();
        excuteFFmpeg(width, height, 1, file, fileNumber);
    }

    private void excuteFFmpeg(final int initWidth, final int initHeight,
                              final int attempt, final File file,
                              final int fileNumber) {

        int width = (initWidth * (100 - 20 * attempt)) / 100;
        int height = (initHeight * (100 - 20 * attempt)) / 100;
        int[] FRAMERATE = {15, 12, 10, 8, 6, 6, 6, 6, 6};
        int frame = FRAMERATE[attempt];

        if (width <= 0 || height <= 0) {
            Log.e(TAG, "it failed compress trial, N : " + attempt);
            handleDirFile(fileNumber + 1);
            return;
        }

        if (width % 2 != 0)
            width--;
        if (height % 2 != 0)
            height--;

        FFmpeg ffmpeg = FFmpeg.getInstance(this);
        final File dest = new File("/storage/emulated/0/Download/test/out", file.getName());
        String[] cmd = {
                "-y",
                "-i", file.getAbsolutePath(),
                "-c:v", "libx264",
                "-vf", "scale=" + String.valueOf(width) + ":" + String.valueOf(height),
                "-r", String.valueOf(frame),
                "-f", "mp4",
                dest.getAbsolutePath()
        };

        try {
            ffmpeg.execute(cmd, new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.i(TAG, "compressed size : " + dest.length() / KILO_BYTES);
                    if (dest.length() / KILO_BYTES < 200) {
                        Log.i(TAG,"compressed success, N : " + attempt);
                        handleDirFile(fileNumber + 1);
                    } else {
                        excuteFFmpeg(initWidth, initHeight,
                                attempt + 1, file, fileNumber);
                    }


                }

                @Override
                public void onProgress(String message) {
                    Log.d("ffmpeg", message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e(TAG, "failure!");
                    Log.e("ffmpeg", "failure!");
                }

                @Override
                public void onStart() {
                    Log.d("ffmpeg", "start");
                }

                @Override
                public void onFinish() {
                    Log.i("ffmpeg", "finish");
                }

            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }
}
